package com.alexgrig.lesson7;

import java.util.Objects;

public class Employee {
    private String surname;
    private String name;
    private int salary;
    private String birthday;

    public Employee() {
    }

    public Employee(String name, int salary) {
        this.name = name;
        this.salary = salary;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public void work() {
        System.out.println(this.getName() + " is working");
    }

    public int getExpectedSalary() {
        return (int) (salary * 1.2);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || obj.getClass() != this.getClass()) {
            return false;
        }

        Employee temp = (Employee) obj;
        return this.surname.equals(temp.getSurname()) && this.name.equals(temp.getName()) && this.salary == temp.getSalary();
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, surname, birthday) * 31;
    }
}
