package com.alexgrig.lesson17_19_01_14;

public class InsufficiantCapacity extends Exception {
    public InsufficiantCapacity(Bus bus) {
        super("Bus capacity is over. Available: " + (bus.getCapacity() - bus.getUsedCapacity()));
    }
}
