package com.alexgrig.OopAndMethods01;

import java.util.Objects;

public class Meat {
    public static void main(String[] args) {

    }

    private final String type;
    private int weight;

    // конструктор
    public Meat(String type) {
        this.type = type;
        switch (type) {
            case "rare":
                this.weight = 100;
                break;
            case "medium":
                this.weight = 90;
                break;
            case "welldone":
                this.weight = 80;
                break;
            default:
                this.weight = 120;
        }

        }
        // перезаписать метод Object toString:
    @Override // проверка того, что мы действительно корректно перезаписываем метод родителя, а не создаем новый ()
    public String toString () {
        return "meat type = " + this.type + "meat weight = " + this.weight;
    }

    // комбинация клавишь Alt + Ins предлагает перезаписать какой-либо из методов родителя, с настройками.
    // Например, вывод может быть таким:
    /*@Override
    public String toString() {
        return "Meat{" +
                "type='" + type + '\'' +
                ", weight=" + weight +
                '}';
    }*/


    // переопределяем метод Objects equals, самостоятельно:
    /*@Override
    public boolean equals (Object meat) {
        if (this == meat) return true; // если ссылка на один и тот же объект, то тру
        *//*if (!meat.getClass().isInstance(this)) return false; *//*// если сравниваемый объект не является объектом класса.
        // Так же:
        if(meat instanceof Meat) {
            Meat inputMeat = (Meat) meat;
            return this.type.equals(inputMeat.getType()) && this.weight == inputMeat.getWeight();
        }
        else {
            return false;
        }
    }*/

    // Тот же переназначенный equals, но автоматический с комбинацией клавишь Alt + Ins:
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Meat meat = (Meat) o;
        return weight == meat.weight &&
                Objects.equals(type, meat.type);
    }

    @Override
    public int hashCode() {
        return Objects.hash(type, weight);
    }

    private int getWeight()  {
        return weight;
    }

    private String getType()  {
        return type;
    }
}
