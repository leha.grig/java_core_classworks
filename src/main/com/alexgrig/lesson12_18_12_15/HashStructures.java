package com.alexgrig.lesson12_18_12_15;

import java.util.*;

public class HashStructures {
    public static void main(String[] args) {

        User vadim = new User(15, "Vadim");
        User slava = new User(20, "Slava");
        User kolya = new User(45, "Kolya");
        User vasya = new User(12, "Vasya");
        User igor = new User(17, "Igor");
        String[] roles1 = {"admin", "user"};
        String[] roles2 = {"editor", "user"};
        String[] roles3 = {"user"};
        String[] roles4 = {"guest"};
        String[] roles5 = {"editor", "user"};
        Map<User, String[]> userHashMap = new HashMap<>();
        userHashMap.put(vadim, roles1);
        userHashMap.put(slava, roles2);
        userHashMap.put(kolya, roles3);
        userHashMap.put(vasya, roles4);
        userHashMap.put(igor, roles5);

        for (Map.Entry<User, String[]> entry : userHashMap.entrySet()) {
            System.out.println(entry.getKey().getName() + " = " + Arrays.toString(entry.getValue()));
        }

        ArrayList<User> keyList = new ArrayList(userHashMap.keySet());
        for (User user : keyList) {
            System.out.println(user.getName());
        }

        ArrayList<String[]> rolesList = new ArrayList(userHashMap.values());
        for (String[] roles : rolesList) {
            System.out.println(Arrays.toString(roles));
        }

        List<String> strings = Arrays.asList("as", "bb", "cv", "as"
        );

//        System.out.println(users.get());
        System.out.println(aggregateValues(strings));
    }

    public static Map<String, Integer> aggregateValues(List<String> values) {

        Map<String, Integer> resultMap = new HashMap<>();

        /*for (String value: values){
            if (!resultMap.containsKey(value)){
                resultMap.put(value, 1);
            }
            else {
                resultMap.put(value, resultMap.get(value)+1);
            }
        }
        // оптимизация:
        */
        for (String value : values) {
            if (!resultMap.containsKey(value)) {
                resultMap.put(value, 0);
            }
            resultMap.put(value, resultMap.get(value) + 1);

        }
        return resultMap;
    }
}
