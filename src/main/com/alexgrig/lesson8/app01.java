package com.alexgrig.lesson8;

import com.sun.javafx.binding.Logging;

import java.util.Arrays;

public class app01 {

    public static void main(String[] args) {

        System.out.println(String.class.getClassLoader());
        System.out.println(Logging.class.getClassLoader());
        System.out.println(app01.class.getClassLoader());

        Family family = new Family();

        long before = System.currentTimeMillis();
        for (int i = 0; i < 100000; i++) {
            family.addFastPerson(new Human(i));
        }
        long after = System.currentTimeMillis();

        System.out.println((after-before));

    }

}


class Family {
    int counter = 0;

    private Human[] persons = new Human[1];

    void addPerson(Human person){
        persons = Arrays.copyOf(persons, persons.length + 1);
        persons[persons.length-1] = person;
    }

    void addFastPerson(Human person){
        if(counter == persons.length-1){
            persons = Arrays.copyOf(persons, persons.length * 2);
        }
        persons[counter] = person;
        counter++;

    }

}

class Human {
    final int id;

    Human(int id) {
        this.id = id;
    }
}
