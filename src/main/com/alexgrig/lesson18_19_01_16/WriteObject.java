package com.alexgrig.lesson18_19_01_16;

import com.sun.corba.se.impl.orbutil.ObjectWriter;

import java.io.*;
import java.util.Arrays;

public class WriteObject {
    public static void main(String[] args) {
        User user1 = new User ("Kiril", "Kirilov");
        User user2 = new User ("Kirill", "Kirillov");
        User user3 = new User ("Kirrill", "Kirrillov");


        /*try {
            ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(new File("objectFile")));
            oos.writeObject(user1);
            oos.writeObject(user2);
            oos.writeObject(user3);
            oos.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }*/

        // то же с лямбда и стримом:
        try {
            ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(new File("objectFile")));
            Arrays.asList(user1, user2, user3).stream().forEach(e -> {
                try {
                    oos.writeObject(e);
                    oos.flush();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            });

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    static class User implements Serializable { // implements Serializable нужен для того, чтоб разрешить объект для записи в файл.
        // Без этого нельзя записать в файл
        private String name;
        private String surname;

        public User(String name, String surname) {
            this.name = name;
            this.surname = surname;
        }

        public String getName() {
            return name;
        }

        public String getSurname() {
            return surname;
        }

        @Override
        public String toString() {
            return "User{" +
                    "name='" + name + '\'' +
                    ", surname='" + surname + '\'' +
                    '}';
        }
    }
}
