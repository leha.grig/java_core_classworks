package com.alexgrig.arrays01;

import java.util.Arrays;

public class ArraysIntro {
    public static void main1(String[] args) {
//        int[] arr = new int[3]; // способ объявить массив
//        arr[0] = 1;
//        arr[1] = 2;
//        arr[2] = 3;

//        int [] arr = {1,2,3}; // более простой способ объявить такой же массив
//
//        int [] a;
//
//        a = new int[3];

        int[] a = new int[]{1,2,3}; // или так задать массив
//        a[3] = 4; // так делать нельзя, нельзя изменять длину заданного массива
//        a[2] = 5;

        System.out.println(Arrays.toString(a));


    }

    public static void main2(String[] args) {
        int[][] arr = new int [2][];// двумерный массив. Могут быть и многомерные массивы
        arr[0] = new int[]{1,2};
        arr[1] = new int [] {1,2,3,4,5,6};
        System.out.println(Arrays.deepToString(arr)); // метод для красивого вывода многомерных массивов
    }

    public static void main3(String[] args) {
        // поменять местами значения переменных:
        int x = 5; // 101
        int y = 3; // 011

        // способ первый, без временной переменной
//        x = x+y;
//        y = x-y;
//        x = x-y;

        // способ второй, через ^ (ксор)
        x = x^y; // 110 => 6   побитовый ^ ставит 1 только тогда, когда значения побитовые разные, если одинаковые - ставит 0
        y = x^y; // 101 => 5
        x = x^y; // 011 => 3
        System.out.println(x);
        System.out.println(y);

    }

    public static void main4(String[] args) {
        /*int i = 10;
        for (; i>0;){ // можно условия указывать и вне условий цикла
            System.out.println(i);
            i--;
        }*/

    }

}
