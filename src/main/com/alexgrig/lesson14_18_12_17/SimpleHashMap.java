package com.alexgrig.lesson14_18_12_17;

// написать реализацию, тесты и отослать на код-ревью

public interface SimpleHashMap<K, V> {
    void put(K key, V value);
    V get (K key); // return value
    boolean containsKey (K key);
    boolean containsValue (V value);
}
class Entry <K, V>{
    private K key;
    private V value;
    private Entry<K, V> next;
    private Entry (K key, V value){
        this.key = key;
        this.value = value;

    }
    public static <K, V> Entry entry (K key, V value){
        return new Entry <> (key, value);
    }
    public Entry with (Entry <K, V> entry) {
        next = entry;
        return entry;
    }

    public Entry<K, V> getNext() {
        return next;
    }

    public K getKey() {
        return key;
    }

    public V getValue() {
        return value;
    }
}