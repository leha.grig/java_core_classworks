package com.alexgrig.lesson16_19_01_12.model;

import java.util.List;
import java.util.Map;

public interface Portal {

    List<Agent> getUniqueAgents();
    List<Property> getPropertiesByType(Property.PropertyType type);
    Property getMostExpenciveProperty();
    List<Property> getPropertiesByAgent(Agent agent);

    Map<String, Integer> priceByPostCode(String postcode);

}
