package com.alexgrig.firstexample;

public enum Gender { // класс с ограниченным заданным числом объектов
    MALE("this is male"), FEMALE("this is female"), UKNOWN("this is unknown"); // возможные объекты класса
    private String description;
    Gender (String description){ // конструктор класса
        this.description = description;
    }
    public String getDescription(){
        return this.description;
    }
}
