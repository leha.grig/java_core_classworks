package com.alexgrig.lesson11_12_18_12_08_collectionsAndOthers;

import java.util.*;

public class ListExample {

    public static void main(String[] args) {

//        List<Integer> list = new ArrayList<>();
//        list.add(1);
//        list.add(2);
//        list.addAll(Collections.singletonList(1));
//        System.out.println(list);
//        list.add(0,5);
//        System.out.println(list);
//        list.addAll(0, Arrays.asList(6,7,8));
//        System.out.println(list);
//        list.remove(1);
//        list.remove(new Integer(1));
//        list.remove(new Integer(1));
//        list.remove(new Integer(1));
//        System.out.println(list);

        List list = new LinkedList();
        list.add(new Integer(1));
        for (Object o : list) {
            Integer o1 = (Integer) o;
            System.out.println(o1++);
        }


    }
}
