package com.alexgrig.firstexample;

import com.alexgrig.firstexample.documents.Document; // если класс в другой папке, то импорт

public final class Person {
    private final String name;
    private final int age;
    private final Gender gender;
    private Document[] documents;

    public static void main(String[] args) {

    }
    public Person (String name, int age, Gender gender) {
        this.name = name;
        this.age = age;
        this.gender = gender;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public Gender getGender() {
        return gender;
    }

    public Document[] getDocuments() {
        return documents;
    }

    public void setDocuments(Document[] documents) {
        this.documents = documents;
    }
}
