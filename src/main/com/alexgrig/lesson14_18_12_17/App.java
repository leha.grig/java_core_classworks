package com.alexgrig.lesson14_18_12_17;

public class App {
    public static void main(String[] args) {
        Entry head = Entry.entry("key1", 12);
        head.with(Entry.entry("key2", 15))
                .with(Entry.entry("key3", 34));

        print(head);

    }
    static void print (Entry head){
        Entry entry = head;
        while(entry != null){
            System.out.println(entry.getKey() + " " + entry.getValue());
            entry = entry.getNext();
        }
    }
}


