package com.alexgrig.lesson15_18_12_19;

import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.Date;

public class DateTimeApi {
    public static void main(String[] args) {
        LocalTime time1 = LocalTime.of(21, 15);
        System.out.println(time1);

        Date date = new Date(-453563);

        Instant now = Instant.now();
        Instant instant = Instant.ofEpochMilli(date.getTime());

        //LocalDate/ LocalTime/ LocalDateTime
        LocalDate now1 = LocalDate.now();
        LocalTime timeInParis = LocalTime.now(ZoneId.of("Europe/Paris"));
        LocalDateTime dayAndTimeInParis = LocalDateTime.of(now1, timeInParis);

        LocalDateTime localDateTime = LocalDateTime.now()
                .withYear(2018)
                .withMonth(12)
                .withDayOfMonth(31);
        System.out.println(localDateTime);


        System.out.println(ZonedDateTime.now());
        System.out.println(Instant.now().toEpochMilli());
        System.out.println(date.toInstant());

        //преобразование строки своего формата в дату, затем в кол-во мс (UTC Timestamp)
        LocalDate birthDate = LocalDate.parse("21/09/2005", DateTimeFormatter.ofPattern("dd/MM/yyyy"));
        LocalDateTime birthDateTime = birthDate.atStartOfDay(); // добавляем к дате время - начало суток

        ZonedDateTime birthDateTimeZ  = birthDateTime.atZone(ZoneId.systemDefault()); // добавляем к дате-времени временную зону
        // (иначе нельзя преобразовать в Инстант - метку на временной шкале)
        long ms = birthDateTimeZ.toInstant().toEpochMilli();

        System.out.println(ms);


        String s = getDateDiffenence("01/01/2018");
        System.out.println(s);

    }

    //получить разницу между датой и сегодняшним числом в формате строки: года, месяцы, дни
    public static String getDateDiffenence(String date) {

        LocalDate now = LocalDate.now();
        LocalDate ldtBirthDate = LocalDate.parse(date, DateTimeFormatter.ofPattern("dd/MM/yyyy"));

        Period period = Period.between(ldtBirthDate, now);

        // разница в выводе названия периода в зависимости от количества тернарными операторами:
        String yearStr = period.getYears()>4 || period.getYears() == 0 ? " лет " : period.getYears() != 1 ?" года " : " год ";
        String monthsStr = period.getMonths()>4 || period.getMonths() == 0 ? " месяцев " : period.getMonths() != 1 ? " месяца " : " месяц ";
        String daysStr = period.getDays()>4 || period.getDays() == 0 ? " дней." : period.getDays() != 1 ? " дня." : " день.";

        return "Разница: " + period.getYears() + yearStr + period.getMonths() + monthsStr + period.getDays() + daysStr;
    }
}
