package com.alexgrig.lesson12_18_12_15;

import java.util.Iterator;
import java.util.List;
import java.util.Spliterator;
import java.util.function.Consumer;

public class Company implements Iterable<Employee> {
    private List<Employee> employees;


    public Company(List<Employee> employees) {
        this.employees = employees;
    }

    @Override
    //реализуем метод iterator из интерфейса Iterable, возвращает объект Iterator
    // Имплементить Iterable нужно, чтоб класс Company стал перебираемый, как List - чтоб можно было
    // по некоторому правилу перебирать его циклом Foreach (см.CompanyConstructor)
    public Iterator iterator() {

        // создание нового анаонимного класса Iterator<Employee> и сразу его объекта (без создания
        // нового класса нельзя, поскольку Iterator это интерфейс - на его основе нельзя создать объекты без класса,
        // который имплементит интерфейс)
        return new Iterator<Employee>() {
            int count = 0;  // это поле нужно создавать в методе, а не в классе, чтоб при каждом новом обращении к
            // методу счетчик обнулялся
            @Override
            public boolean hasNext() {

                return count < employees.size();
            }

            @Override
            public Employee next() {

                return employees.get(count++);


            }
        };
    }


}
