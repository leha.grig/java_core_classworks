package com.alexgrig.strings;

import java.util.Arrays;

public class StringLesson {
    public static void main(String[] args) {
        String str1 = stringRevert("qwerty");
        System.out.println(str1);
    }
    // инвертировать строку:
    private static String revert(String value) {
        char[] chars = value.toCharArray();
        char tmp;
        for (int i=0; i < chars.length/2; i++) {
            tmp = chars[i];
            chars[i] = chars[chars.length-i-1];
            chars[chars.length-i-1] = tmp;
        }
        return String.valueOf(chars);
    }
    private static String stringRevert (String str) {
        char[] strArr = str.toCharArray();
        // второй метод инвертировать массив (первый - циклом for до половины массива, как в пакете arrays01 и выше)
        int a = 0;
        int b = strArr.length-1;
        while (a<b){
            char temp= strArr[a];
            strArr[a] = strArr[b];
            strArr[b] = temp;
            a++;
            b--;
        }
//        String str2 = Arrays.toString(strArr);
        return String.valueOf(strArr);
    }
}
