package com.alexgrig.lesson11_12_18_12_08_collectionsAndOthers;

import java.util.*;

public class ListService {
    public static void main(String[] args) {
        List<Integer> list = new ArrayList<>();// в виде листов лучше работать на контракте - указывать вид List, а не ArrayList или LinkedList -
        // чтоб можно было создавать разные листы: ArrayList, LinkedList
        // классы-родители Листов определены с типом Generic <E> - любой элемент. В данном случае мы задали тип Integer

        list.add(1);
        list.add(2);
        System.out.println(list);
        ArrayList<Integer> list2 = new ArrayList<>(list);
        list2.add(3);
        list.addAll(list2);
        System.out.println(list2);

        list2.remove(2);
        System.out.println(list2);
        System.out.println(list2.size());

        List<Integer> list3 = Arrays.asList(2, 1, 5, 4, 3);
        System.out.println(list3);

        Collections.sort(list3);
        System.out.println(list3);

        String[] stringArray = new String[0];
        List<String> linkedList = new LinkedList<>();
        List<String> arrayList = new ArrayList<>();

        long startCreatingSA1 = System.currentTimeMillis();
        generateArray(stringArray);
        System.out.println("String array creating time1: " + (System.currentTimeMillis() - startCreatingSA1));

        long startCreatingSA2 = System.currentTimeMillis();
        generateArray2(stringArray);
        System.out.println("String array creating time2: " + (System.currentTimeMillis() - startCreatingSA2));

        long startCreatingLL = System.currentTimeMillis();
        generate(linkedList);
        System.out.println("Linked list creating time: " + (System.currentTimeMillis() - startCreatingLL));

        long startCreatingAL = System.currentTimeMillis();
        generate(arrayList);
        System.out.println("Array list creating time: " + (System.currentTimeMillis() - startCreatingAL));

        long start = System.currentTimeMillis();
        removeRange(linkedList);
        System.out.println("Linked list remove time : " + (System.currentTimeMillis() - start));

        long start2 = System.currentTimeMillis();
        removeRange(arrayList);
        System.out.println("Array list remove time : " + (System.currentTimeMillis() - start2));


    }

    static void removeRange(List<String> elem) {
        for (int i = 20_000; i < 50_000; i++) {
            elem.remove(20000);
        }
    }


    static List<String> generate(List<String> elems) {
        for (int i = 0; i < 100_000; i++) {
            elems.add(String.valueOf(i));
        }
        return elems;
    }

    static String[] generateArray(String[] array) {
        for (int i = 0; i < 100_000; i++) {
            array = Arrays.copyOf(array, array.length + 1);
            array[i] = (String.valueOf(i));
        }
        return array;
    }
    static String[] generateArray2(String[] array) {
        for (int i = 0; i < 100_000; i++) {
            String[] temp =  new String[i+1];
            System.arraycopy(array, 0, temp, 0, i);
            temp[i] = (String.valueOf(i));
            array = temp;
        }
        return array;
    }

}
