package com.alexgrig.lesson17_19_01_14;

public class NotEnough extends Exception{
    public NotEnough(Bus bus) {
        super("Bus has only: " + bus.getUsedCapacity());
    }
}
