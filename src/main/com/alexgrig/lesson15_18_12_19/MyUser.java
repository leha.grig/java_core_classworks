package com.alexgrig.lesson15_18_12_19;

import java.util.Date;

public class MyUser {
    private String name;
    private Date birthDate;
    private Date salaryDate;
    private int salary;

    public MyUser(String name, Date birthDate, Date salaryDate) {
        this.name = name;
        this.birthDate = birthDate;
        this.salaryDate = salaryDate;
    }

    public void setSalaryDate(Date salaryDate) {
        this.salaryDate = salaryDate;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    public String getName() {
        return name;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public Date getSalaryDate() {
        return salaryDate;
    }

    public int getSalary() {
        return salary;
    }
}
