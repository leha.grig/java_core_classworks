package com.alexgrig.lesson16_19_01_12;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class LambdaService {
    public static void main(String[] args) {
        List<String> values = Arrays.asList("Alexey", "Dima", "Petr");
        /*sort(values, new Comparator<String>(){
            @Override
            public int compare(String o1, String o2) {
                return o1.length()-o2.length();
            }
        });*/

        // та же запись, только с помощью лямбда-выражений
        sort(values, (a,b) -> a.length()-b.length());

        System.out.println(values);

        System.out.println(countDuplicates("a", "asw", "a", "qwe", "ds"));
    }
    static <T> List<T> sort(List<T> values, Comparator<T> comparator){
        Collections.sort(values, comparator);
        return values;
    }

    //посчитать дупликаты
    static <T> int countDuplicates (T value, T... args){
        int count = 0;
        for (T elem:
             args) {
            count += elem.equals(value)? 1:0;
        }
        return count;
    }
}
