package com.alexgrig.OopAndMethods01;

public class Restourant {
    public static void main(String[] args) {
        Meat meat1 = new Meat ("rare");
        Meat meat2 = new Meat ("rare");
        System.out.println(meat1);
        System.out.println(meat2);
        System.out.println(meat1.equals(meat2));
    }
}
