package com.alexgrig.lesson18_19_01_16;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class BufferedFileWriter {
    public static void main(String[] args) {
        File file1 = new File("file1");
        try {
            file1.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println(file1.getName());
        System.out.println(file1.getPath());
        System.out.println(file1.getParent());
        System.out.println(file1.getAbsoluteFile().getParent());
        System.out.println(file1.getAbsolutePath());
        try {
            System.out.println(file1.getCanonicalPath());
        } catch (IOException e) {
            e.printStackTrace();
        }

        FileWriter fw = null;
        try {
            fw = new FileWriter (file1);
            fw.append("First line");
            fw.append("Second line").flush();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                fw.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


}
