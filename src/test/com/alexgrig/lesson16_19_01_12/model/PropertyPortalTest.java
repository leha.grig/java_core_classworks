package com.alexgrig.lesson16_19_01_12.model;

import org.junit.Assert;
import org.junit.Before;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

public class PropertyPortalTest {
    List<Property> properties;
    @Before
    public void beforeEach (){
        properties = Arrays.asList(
                new Property("123", Property.PropertyType.FLAT, 100000, new Agent("silver")),
                new Property("234", Property.PropertyType.HOUSE, 250000, new Agent("silver")),
                new Property("asd", Property.PropertyType.HOUSE, 300000, new Agent("black")),
                new Property("675", Property.PropertyType.FLAT, 450000, new Agent("gold")),
                new Property("SFd12", Property.PropertyType.HOUSE, 350000, new Agent("white"))

        );
    }

    @Test
    public void shouldGetUniqueAgents_byProperties() {
        // given

        //when
        PropertyPortal portal = new PropertyPortal(properties);
        List<Agent> agents = portal.getUniqueAgents();
        // then
        List<Agent> expectedAgents = Arrays.asList(
                new Agent("silver"),
                new Agent("black"),
                new Agent("gold"),
                new Agent("white")
        );

        // в этом случае последовательность в листе агентов имеет значение:
        // Assert.assertThat(agents, is(expectedAgents));

        //последовательность не имеет значения:
        expectedAgents.forEach(e->Assert.assertTrue(agents.contains(e)));
    }

    @Test
    public void shouldGetProperties_byType() {
        // given

        //when
        PropertyPortal portal = new PropertyPortal(properties);
        List<Property> result = portal.getPropertiesByType(Property.PropertyType.FLAT);
        // then
        List<Property> expectedProperties = Arrays.asList(
                new Property("123", Property.PropertyType.FLAT, 100000, new Agent("silver")),

                new Property("675", Property.PropertyType.FLAT, 450000, new Agent("gold"))

        );
        // в этом случае последовательность в листе агентов имеет значение:
        // Assert.assertThat(agents, is(expectedAgents));

        //последовательность не имеет значения:
        expectedProperties.forEach(e->Assert.assertTrue(result.contains(e)));
    }

}