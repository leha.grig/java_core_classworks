package com.alexgrig.firstexample;

import java.util.Scanner;

public class Example2 {
    public static void main1(String[] args) { // метод main должен быть только один, только он вызывается в консоль
        Scanner in = new Scanner(System.in);
        int a = in.nextInt();
        int b = in.nextInt();
        System.out.println(a > b ? a : b);
        int c = in.nextInt();
        int d = in.nextInt();
        int e = in.nextInt();
        if (c < d && c < e) {
            System.out.println(c);
        } else if (d < e) {
            System.out.println(d);
        } else {
            System.out.println(e);
        }
    }

    // проверить, сколько цифр в строке-числе, вывести да, если 2 символа, нет, если другое количество
    public static void main2(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String number = scanner.nextLine();
        System.out.println(number.matches("[0-9]{2}") ? "Yes" : "No"); // регулярные выражения (задают параметры строки): [] говорит об одном символе, {}говорит о количестве символов,
        // + - некоторое количество таких символов. например, [0-9]{2} то же, что [0-9][0-9] (два символа от 0 до 9 каждый); [0-9]+ - один
        // или несколько символов от 0 до 9 каждый.
    }

    // метод выводит число Фиббоначи по индексу (домашка!!!)
    public static void main(String[] args) {
        int a = 7;
        System.out.println(fib2(a));
    }

    static int fib(int index) { // рекурсия
        if (index <= 2) return 1;
        return fib(index - 1) + fib(index - 2);
    }
    static int fib2 (int index){ // суммация
        int a = 1;
        int b = 1;
        int c = 1;
        for (int i = 3; i<=index; i++){
            c = a+b;
            a = b;
            b = c;
        }
        return c;
    }

}
