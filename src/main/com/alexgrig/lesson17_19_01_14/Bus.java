package com.alexgrig.lesson17_19_01_14;

public class Bus {
    private String driverName;
    private int capacity;
    private int usedCapacity;

    public Bus(String driverName, int capacity) {
        this.driverName = driverName;
        this.capacity = capacity;
    }

    public void load(int addCapacity) throws InsufficiantCapacity{
        if (usedCapacity + addCapacity > capacity){
            throw new InsufficiantCapacity(this);
        }
        usedCapacity += addCapacity;
        System.out.println(this);
    }

    public void upload(int reduceCapacity) throws NotEnough{
        if (usedCapacity - reduceCapacity < 0) {
            throw new NotEnough(this);
        }
        usedCapacity -= reduceCapacity;
        System.out.println(this);
    }

    public String getDriverName() {
        return driverName;
    }

    public int getCapacity() {
        return capacity;
    }

    public int getUsedCapacity() {
        return usedCapacity;
    }

    @Override
    public String toString() {
        return "Bus{" +
                "driverName='" + driverName + '\'' +
                ", capacity=" + capacity +
                '}';
    }
}
