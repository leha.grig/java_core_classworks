package com.alexgrig.lesson9_18_12_03_practics;

public class Mark {
    private final Subject subject;
    private int mark;

    public Mark(Subject subject, int mark) {
        this.subject = subject;
        this.mark = mark;
    }

    @Override
    public String toString() {
        return subject.name() +
                ": " + mark;
    }

    public Subject getSubject() {
        return subject;
    }

    public int getMark() {
        return mark;
    }

    public void setMark(int mark) {
        this.mark = mark;
    }
}
