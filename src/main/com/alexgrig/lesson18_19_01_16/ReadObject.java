package com.alexgrig.lesson18_19_01_16;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;

public class ReadObject {
    public static void main(String[] args) {
        try {
            ObjectInputStream objectFile = new ObjectInputStream(new FileInputStream("objectFile"));
            for (int i = 0; i < 3; i++) {
                System.out.println(objectFile.readObject());
            }

        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
