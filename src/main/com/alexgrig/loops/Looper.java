package com.alexgrig.loops;

import java.util.Arrays;

public class Looper {
    public static void main(String[] args) {
        //arrays
//        int[] arr = {1, 2, 4};
//        int[][] arr2 = {{1, 2}, {3}};

        //loops examples

        //for(int i=0;i<10; i++ | i ^= 2 | i+=2)
        //int i = 0;
        //for(;true; i++)


//        for (int i = 9; i > 0; i-=2) {
//            System.out.println(i);
//        }
//            for (int i = 0; i < 10; i++) {
//                System.out.println(i);
//                i--;


//        int k = 0;
//        for(;;){
//            if(k<10){
//                System.out.println(k);
//                k++;
//            } else {
//                break;
//            }
//        }


        //2d_loop
//        for (int i = 0; i < 10; i++) {
//            for (int j = 0; j < 10; j++) {
//                System.out.print(i < j ? '0' : '1');
//            }
//            System.out.println();
//        }

        //dependant on outer array
//        int[] ints = {0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100};
//        for (int i = 0; i <= ints.length; i++) {
//            for (int j = 0; j < i; j++) {
//                System.out.print(ints[j]);
//            }
//            System.out.println();
//        }


    }

}

