package com.alexgrig.lesson16_19_01_12.model;

import java.util.Objects;

public class Agent {
    private String name;

    public Agent(String name) {
        this.name = name;
    }
    public Agent() {
    }

    public String getName() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Agent agent = (Agent) o;
        return Objects.equals(name, agent.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }
}
