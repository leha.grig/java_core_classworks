package com.alexgrig.OopAndMethods01;

public class App {

    public static void main(String[] args) {
        A a = new A();
        System.out.println(a.value);
    }
}

class A {
    int value = 5;

    // блок инициализации (при создании нового объекта вызывается после конструктора - т.е. значение будет отсюда)
    {
        System.out.println("int block");
        this.value = 10;
    }

    // статический блок инициализации
    static {
        System.out.println("static block");
    }

    // конструктор
    A(){
        System.out.println("constructor");
    }

    // перегруженный конструктор (с аргументом)
    A(int value){
        System.out.println("constructor2");
        this.value = value;
    }
}
