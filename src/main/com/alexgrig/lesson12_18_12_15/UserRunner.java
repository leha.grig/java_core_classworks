package com.alexgrig.lesson12_18_12_15;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class UserRunner {
    public static void main(String[] args) {
        User user1 = new User(15, "Vadim");
        User user2 = new User(20, "Slava");
        User user3 = new User(17, "Igor");
        System.out.println(user1.compareTo(user2));
        List<User> users = new ArrayList();
        users.add(user1);
        users.add(user2);
        users.add(user3);

      List <User> usersSorted1 = sort(users);
        System.out.println(usersSorted1);

        List <User> usersSorted2 = sort(users, comparator);
        System.out.println(usersSorted2);
    }

    public static <T extends Comparable<T>> List<T> sort (List<T> list){
        List<T> result = new ArrayList<>(list);// клонируем сначала входной Лист, поскольку
        // Collections.sort() мутирует объект, а изначальный объект лучше сохранять неизменным
        Collections.sort(result);
        return result;
    }

    public static Comparator<User> comparator = new Comparator<User>() {
        @Override
        public int compare(User u1, User u2) {
            return u1.getAge()-u2.getAge();
        }
    };

    public static List<User> sort(List<User> list, Comparator<User> comparator) {

        return list;
    }
}
