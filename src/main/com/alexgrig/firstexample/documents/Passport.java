package com.alexgrig.firstexample.documents;

public class Passport implements Document {
    private final String id;
    private final String name;
    private String photo;

    public Passport(String id, String name) {
        this.id = id;
        this.name = name;
    }


    @Override
    public String getId() {
        return this.id;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public String getPhoto() {
        return this.photo;
    }
    public void setPhoto (String photo){
        this.photo = photo;
    }
}
