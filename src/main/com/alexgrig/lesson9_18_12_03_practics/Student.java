package com.alexgrig.lesson9_18_12_03_practics;

import java.lang.reflect.Array;
import java.util.Arrays;

public class Student {
    private final String name;
    private final String surname;
    private Mark[] marks;

    public Student(String name, String surname) {
        this.name = name;
        this.surname = surname;
        this.marks =  MarkGenerator.generateMarks();
    }

    public String getPersonalInfo() {
        return name + " " + surname;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getMarksInfo() {
        return Arrays.toString(marks);
    }

    public Mark[] getMarks() {
        return marks;
    }

    /*public int getSubjectMark(Subject subject) {
        for (Mark elem: marks){
            if (elem.getSubject() == subject){
                return elem.getMark();
            }
        }
        return 0;
    }*/

    public void setMarks(Mark[] marks) {
        this.marks = marks;
    }

    @Override
    public boolean equals (Object obj){
        if (this == obj) {return true;}
        if (obj == null || getClass() != obj.getClass()){return false;}
        Student student = (Student) obj;
        return name.equals(student.getName()) && surname.equals(student.getSurname());
    }

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                '}';
    }
}
