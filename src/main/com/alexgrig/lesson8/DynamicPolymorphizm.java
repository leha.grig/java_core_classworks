package com.alexgrig.lesson8;

public class DynamicPolymorphizm {

    public static void main(String[] args) {



    }

}

abstract class Parent {

    void print(){
        System.out.println("This is " + getName());
    }

    void print(Parent parent){

    }

    abstract String getName();
    abstract void changeName(String name);

}

class Child1 extends Parent{

    public Child1(String name) {
        System.out.println(name);
    }

    private String name = "Child1";

    @Override
    String getName() {

        return name;
    }

    public void changeName(String newName){
        this.name = newName;
    }

}
class Child2 extends Parent{


    public Child2() {
        System.out.println("child2");
    }


    @Override
    String getName() {
        return "Child2";
    }

    @Override
    void changeName(String name) {
        throw new RuntimeException("can't change name");

    }
}