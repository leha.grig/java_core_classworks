package com.alexgrig.lesson15_18_12_19;


import org.junit.Assert;
import org.junit.Test;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

import static org.hamcrest.CoreMatchers.is;


public class UserServiceImplTest {

    private UserService service = new UserServiceImpl();

    @Test
    public void shouldPrintBirthday(){
        MyUser user = new MyUser("", new Date(), null);
        service.printBirthDay(user);
    }

    @Test
    public void shouldReturnNumberOfDaysToNewYear(){
        Date newYearDate = Date.from(LocalDate.of(2020, 12, 31).atStartOfDay(ZoneId.systemDefault()).toInstant());
        int daysLeftToDate = service.getDaysLeftToDate(newYearDate);
        Assert.assertThat(daysLeftToDate, is(13));
    }

    @Test
    public void shouldReturnDifferenceOfBirthdays(){
        MyUser user1 = new MyUser("", Date.from(LocalDate.of(1999, 10, 10).atStartOfDay(ZoneId.systemDefault()).toInstant()), null);
        MyUser user2 = new MyUser("", Date.from(LocalDate.of(1999, 10, 18).atStartOfDay(ZoneId.systemDefault()).toInstant()), null);
        int daysLeftToDate = service.getDifferenceBetweenBirthdays(user1, user2);

        Assert.assertThat(daysLeftToDate, is(-8));
    }


}