package com.alexgrig.lesson16_19_01_12;

import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;

//должен принимать список элементов, функциональный интерфейс, выполнял действие на элементе, засыпал на нем, потом выполнял следующее действие.
// Время засыпания получает как параметр
// Ту же задачу смотри реализованной в App01
public class ThreadUtil {
    public static void main(String[] args) {
        List<String> strings = Arrays.asList("qwe", "we", "wefd");
       /* execute(strings, new Consumer() { // тут создание анонимного класса
            @Override
            public void accept(Object o) {
                System.out.println(o);
                // Thread.sleep выкидывает исключение, потому через трай-кэтч
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });*/
        execute(strings, (s)-> { // то же с лямбда-выражением

                System.out.println(s);
                //Thread.sleep выкидывает исключение, потому через трай-кэтч
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
        });
    }

    static <T> void execute (List<T> list, Consumer consumer){
        list.forEach(consumer);
    }
}

