package com.alexgrig.lesson16_19_01_12;

import static java.util.Optional.*;

public class OptionalService {

    public static void main(String[] args) {

        Property prop1 = new Property("prop1", 100_000);

        String name = ofNullable(prop1)
                .map(Property::getName)
                .orElse("");

        System.out.println(name);


    }


}
