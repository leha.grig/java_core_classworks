package com.alexgrig.lesson17_19_01_14;

import java.io.FileNotFoundException;

public class CustomCheckedException {
    public static void main(String[] args) {
        try {
            doSomething(); // обработка ошибки в момент вызова метода
        } catch (CheckedException e) {
            System.err.println(e);
        }

        System.out.println("Finish");
    }

    public static void doSomething() throws CheckedException { // пробрасываем ошибку вверх -
        // нет необходимости обрабатывать тут, обрабатываем на моменте вызова метода
        throw new CheckedException("Oooops");
    }

    static class CheckedException extends FileNotFoundException {
        public CheckedException(String s) {
            super(s);
        }
    }
}
