package com.alexgrig.firstexample;

import com.alexgrig.firstexample.documents.Document;
import com.alexgrig.firstexample.documents.Passport;

import java.util.Arrays;
import java.util.List;


public class PersonGenerator {
    public static void main(String[] args) {
        Person euha = new Person("Euha", 25, Gender.MALE);
        Document euhaPas = new Passport("123456", "Euha");
        List<Document> documents = Arrays.asList(euhaPas);
        euha.setDocuments((Document[])documents.toArray());
        String info = "Name = " + euha.getName() + "\n Age = " + euha.getAge() + "\n Gender = " + euha.getGender().getDescription() + "\n PassportID = " + euha.getDocuments()[0].getId();
        System.out.println(info);
    }
}
