package com.alexgrig.lesson9_18_12_03_practics;

import java.util.Objects;

public class Teacher {
    private final String name;
    private final String surname;
    private final Subject subject;

    public Teacher(String name, String surname, Subject subject) {
        this.name = name;
        this.surname = surname;
        this.subject = subject;
    }
    public void putMark (Student student, int mark){
        for (Mark elem: student.getMarks()){
            if (elem.getSubject() == subject){
                elem.setMark(mark);
            }
        }
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public Subject getSubject() {
        return subject;
    }
    public String showSubject() {
        return subject.name();
    }
    public Student compareStudents(Student stud1, Student stud2){
        int mark1 = 0, mark2 = 0;
        for (Mark elem: stud1.getMarks()){
            if (elem.getSubject() == subject){
                mark1 = elem.getMark();
            }
        }
        for (Mark elem: stud2.getMarks()){
            if (elem.getSubject() == subject){
                mark2 = elem.getMark();
            }
        }
        if (mark1 != 0 && mark2 != 0){
            if (mark1 > mark2){
                return stud1;
            }
            if (mark2 > mark1){
                return stud2;
            }
        }
        return null;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Teacher teacher = (Teacher) o;
        return Objects.equals(name, teacher.name) &&
                Objects.equals(surname, teacher.surname) &&
                subject == teacher.subject;
    }

    @Override
    public String toString() {
        return "Teacher{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", subject=" + subject +
                '}';
    }
}
