package com.alexgrig.lesson12_18_12_15;

import sun.reflect.generics.tree.Tree;

import java.util.*;

public class TreeStructures {
    private Set<User> users;

    public static void main(String[] args) {
        List<User> users = Arrays.asList(
                new User(15, "Vadim"),
                new User(20, "Slava"),
                new User(45, "Kolya"),
                new User(12, "Vasya"),
                new User(17, "Igor")
        );
        Set<User> sortedUsers = new TreeSet<>(users);
        System.out.println(sortedUsers);

        NavigableSet<User> sortedUsersNav = new TreeSet<>(users);

        Set <User> elderUsers = sortedUsersNav.tailSet(new User(20, ""));
        System.out.println(elderUsers);

        Set <User> middleUsers = sortedUsersNav.subSet(new User(17, ""), new User (25, ""));
        System.out.println(middleUsers);

//        Set <User> sortedUsersWithComparator = new TreeSet<>(Comparator.comparingInt(o -> o.getId))
    }

}
