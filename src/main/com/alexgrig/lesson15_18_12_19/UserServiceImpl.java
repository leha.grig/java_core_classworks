package com.alexgrig.lesson15_18_12_19;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.List;

public class UserServiceImpl implements UserService {
/*    private final static String DATE_FORMAT = "dd/MM/yyyy";
    @Override
    public void printBirthDay(MyUser user) {

        //Simple day format case
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        System.out.println(dateFormat.format(user.getBirthDate()));

        // DateTimeFormatter
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DATE_FORMAT);

        Instant instant = user.getBirthDate().toInstant();
        ZonedDateTime zone = ((Instant) instant).atZone(ZoneId.systemDefault());
        LocalDate localDate = zone.toLocalDate();

        System.out.println(localDate.format(formatter));
    }

    @Override
    public void printBirthDay(Date date) {

    }

    @Override
    public int getDaysLeftToDate(Date date) {
        LocalDate nowDate = LocalDate.now();
        LocalDate gotDate = LocalDate.ofEpochDay(date.getTime());
//        System.out.println(gotDate.minusDays(nowDate));

        return 0;
    }
    */

    private final static String DATE_FORMAT = "dd/MM/yyyy";

    //dd/MM/yyyy
    @Override
    public void printBirthDay(MyUser user) {
        //SimpleDateFormat
        SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);
        System.out.println(dateFormat.format(user.getBirthDate()));
        //DateTimeFormatter
        printBirthDay(user.getBirthDate());
    }

    @Override
    public void printBirthDay(java.util.Date date) {
        printBirthDay(date, DATE_FORMAT);
    }

    @Override
    public void printBirthDay(List<MyUser> users) {
        for (MyUser user : users) {
            printBirthDay(user);
        }
    }

    @Override
    public void printBirthDay(MyUser user, String format) {
        printBirthDay(user.getBirthDate(), format);
    }

    @Override
    public int getDaysLeftToDate(java.util.Date date) {
        LocalDate toDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        LocalDate now = LocalDate.now();
//        return toDate.minusDays(now.getDayOfYear()).getDayOfYear();
        long between = ChronoUnit.DAYS.between(toDate, now);
        long l = Duration.between(toDate.atStartOfDay(), now.atStartOfDay()).toDays();
        return (int) l;
    }

    @Override
    public int getDifferenceBetweenBirthdays(MyUser user1, MyUser user2) {

        LocalDate user1BirthDay = convert(user1.getBirthDate());
        LocalDate user2BirthDay = convert(user2.getBirthDate());

        return user1BirthDay.getDayOfYear() - user2BirthDay.getDayOfYear();
    }

    private LocalDate convert(java.util.Date date){
        return date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
    }

    private void printBirthDay(java.util.Date date, String format) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(format);
        Instant instant = date.toInstant();
        ZonedDateTime zone = instant.atZone(ZoneId.systemDefault());
        LocalDate localDate = zone.toLocalDate();
        System.out.println(localDate.format(formatter));
    }




}
