package com.alexgrig.lesson16_19_01_12;

import java.util.Arrays;
import java.util.function.Supplier;

public class App01 {

    public static void main(String[] args) {


        PersonWorker worker = (e) -> {};
        worker.doSmthWithPerson(() -> new Person());
        worker.doSmthWithPerson(Person::new);

        Arrays.asList("a","b","c")
                .forEach(s -> {
                    System.out.println(s);
                    sleep(100);
                });


    }

    static void sleep(int time) {
        try {
            Thread.sleep(time);

        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }





}

interface PersonWorker {
    void doSmthWithPerson(Supplier<Person> person);
}

class Person{
    public Person() {
    }
    public Person(String s) {
    }
}
