package com.alexgrig.lesson16_19_01_12;

import java.util.Arrays;
import java.util.Objects;
import java.util.function.Consumer;

public class DuplicateService {
    public static void main(String[] args) {
        Property value = new Property ("a", 100);

        //вызываем метод countDuplicates, в котором сразу реализовываем интерфейс Equalizer лямбда-выражениями.
        // Представить интерфейс лямбда-выражением можно только тогда, когда это функциональный интерфейс -
        // т.е. такой, у которого есть только один абстрактный метод (который мы и заменяем выражением)
        int result = countDuplicates(value, (a,b)->a.equals(b), // (a,b)->a.equals(b) может быть заменено на Property::equals - вызов метода
                new Property ("a", 100),
                new Property ("bd", 104),
                new Property ("wd", 100)
                );
        System.out.println(result);
    }
    static <T> int countDuplicates (T value, Equalizer <T> equalizer, T... arr){
        int count = 0;
        for (T elem:
             arr) {
            count += equalizer.equal(value, elem)? 1:0;
        }
        return count;
    }
}

@FunctionalInterface
interface Equalizer <T>{
    boolean equal(T a, T b);
}

class Property {
    private String name;
    private int price;

    public Property(String name, int price) {
        this.name = name;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public int getPrice() {
        return price;
    }

    @Override
    public String toString() {
        return "Property{" +
                "name='" + name + '\'' +
                ", price=" + price +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Property property = (Property) o;
        return price == property.price &&
                Objects.equals(name, property.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, price);
    }
}