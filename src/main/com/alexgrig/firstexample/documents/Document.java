package com.alexgrig.firstexample.documents;

public interface Document {
    String getId();

    String getName();

    String getPhoto();
}
