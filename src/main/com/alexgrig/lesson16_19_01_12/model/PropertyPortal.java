package com.alexgrig.lesson16_19_01_12.model;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class PropertyPortal implements Portal{
    final private List<Property> properties;

    //DAO
    public PropertyPortal(List<Property> properties) {
        this.properties = properties;
    }

    @Override
    public List<Property> getPropertiesByAgent(Agent agent) {
        return properties.stream()
                .filter(e->e.getAgent().equals(agent))
                .collect(Collectors.toList());
    }

    @Override
    public List<Agent> getUniqueAgents() {
        return properties.stream()
                .map(Property::getAgent)// выбираем Агента из каждого проперти
                .distinct() // возвращаем стрим с уникальными агентами (по иквелсу - нужно прописать в агенте)
                .collect(Collectors.toList()); // собираем в коллексцию выбранных агентов
    }

    @Override
    public List<Property> getPropertiesByType(Property.PropertyType type) {
        return properties.stream()
                .filter(property -> property.getType() == type)
                .collect(Collectors.toList());
    }

    @Override
    public Property getMostExpenciveProperty() {
        return properties.stream()
                .max(Comparator.comparingInt(Property::getPrice)).orElse(null);
    }


    // Дописать: возвращает ???
    // дописать тесты на методы
    @Override
    public Map<String, Integer> priceByPostCode(String postcode) {
        Map<String, List<Property>> propertiesByPostCode = properties.stream()
                .collect(Collectors.groupingBy(Property::getPostcode));

        return null;
    }


}
