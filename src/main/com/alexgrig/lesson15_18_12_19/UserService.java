package com.alexgrig.lesson15_18_12_19;

import java.util.Date;
import java.util.List;

public interface UserService {

    void printBirthDay(MyUser user);
    void printBirthDay(Date user);
    void printBirthDay(List<MyUser> user);
    void printBirthDay(MyUser user, String format);

    int getDaysLeftToDate(Date date);
    int getDifferenceBetweenBirthdays(MyUser user1, MyUser user2);

}
