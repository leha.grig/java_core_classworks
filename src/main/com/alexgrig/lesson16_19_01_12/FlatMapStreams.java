package com.alexgrig.lesson16_19_01_12;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class FlatMapStreams {

    public static void main(String[] args) {

        List<List<String>> bookRows = new ArrayList<>();

        bookRows.add(Arrays.asList("abcd", "efg", "hij"));
        bookRows.add(Arrays.asList("klmn", "opqr", "st"));
        bookRows.add(Arrays.asList("uvw", "x", "yz"));

        System.out.println(bookContent(bookRows)); // [abcd, efg, hij, klmn, opqr, st, uvw, x, yz]



    }

    static List<String> bookContent(List<List<String>> bookRows){
        return bookRows.stream() // на этом этапе в стриме элементы - это коллекции  List<String>
                .flatMap(Collection::stream) // flatMap берт элементы из стрима в аргументах и выводит их в основной стрим
                .collect(Collectors.toList()); // собираем элементы стрима опять в коллекцию, теперь - одноуровневая
    }
}
