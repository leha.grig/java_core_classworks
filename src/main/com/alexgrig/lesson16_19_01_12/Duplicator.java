package com.alexgrig.lesson16_19_01_12;

import java.util.function.BiFunction;
import java.util.stream.Stream;

public class Duplicator {

    public static void main(String[] args) {

        System.out.println(countDuplicates(1, 1,2,3,4,5,1));
        System.out.println(countWithLambda(1, (integer, integer2) -> integer.equals(integer2) ? 1 : 0, 1,2,3,4,5,1));

        long integer = Stream.iterate(10, y -> {
            System.out.println("inside" + y);
            return y - 1;
        }).limit(100).count();

        System.out.println(integer);


    }

    static <T> int countDuplicates(T val, T... arr){
        int counter = 0;
        for (T t : arr) {
            counter += val.equals(t) ? 1 : 0;
        }
        return counter;
    }

    static <T> int countWithLambda(T val, BiFunction<T, T, Integer> func, T... arr){
        int counter = 0;
        for (T t : arr) {
            counter += func.apply(val, t);
        }
        return counter;
    }




}
