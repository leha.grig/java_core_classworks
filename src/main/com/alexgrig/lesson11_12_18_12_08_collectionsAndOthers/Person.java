package com.alexgrig.lesson11_12_18_12_08_collectionsAndOthers;

import java.util.Arrays;

// Generic <T> обозначает любой тип, который должен быть строго определен при создании объекта.
// Если <T extends Number> - то тип может быть уже только числовой
public class Person<T extends Number> {

    private final String name;
    private String[] cars;
    private T nationality;

    public Person(String name) {
        this.name = name;
    }

    public Person(String name, T nationality) {
        this.name = name;
        this.nationality = nationality;
    }

    public String getName() {
        return name;
    }

    public String[] getCars() {
        return cars;
    }

    public void setCars(String[] cars) {
        this.cars = cars;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Person person = (Person) o;

        if (name != null ? !name.equals(person.name) : person.name != null) return false;
        // Probably incorrect - comparing Object[] arrays with Arrays.equals
        return Arrays.equals(cars, person.cars);
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + Arrays.hashCode(cars);
        return result;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", cars=" + Arrays.toString(cars) +
                '}';
    }


}
