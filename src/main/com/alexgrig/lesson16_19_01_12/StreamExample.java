package com.alexgrig.lesson16_19_01_12;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class StreamExample {
    public static void main(String[] args) {
        int symbols = countSymbols (Arrays.asList("Sergey", "Nick", "Alex"));
        System.out.println("number of symbols = " + symbols);
        System.out.println(maxSymbols(Arrays.asList("Sergey", "Nick", "Alex")));
    }


    static int countSymbols (List <String> elems){
// можно вернуть этот эелемент:
        int elementSum = elems.stream()
                .reduce(String::concat)
                .orElse("")
                .length();
        // или:
        return elems.stream()
                .map(String::length)
                .reduce((a,b)->a+b)
                .get(); // нет проверки на null!!!
    }

    // найти строку с максимальным числом символов
    static String  maxSymbols (List <String> elems){
        return elems.stream()
                .max((a,b)->a.length()-b.length()).orElse(""); // (a,b)->a.length()-b.length() можно заменить на метод Компаратора Comparator.comparingInt(String::length)

        // .orElse("") вместо .get() обеспечивает отсутствие необходимости проверки на null

        // или так, через метод sorted:
        /*return elems.stream()
                .sorted((a,b)->b.length()-a.length()).findFirst().orElse("");*/
    }
}
