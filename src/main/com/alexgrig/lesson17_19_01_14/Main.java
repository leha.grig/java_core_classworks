package com.alexgrig.lesson17_19_01_14;

public class Main {
    public static void main(String[] args) {
        Bus bus = new Bus("Euha", 100);
        try {
            bus.load(70);
            bus.upload(20);
            brokenSomthing();// Ctrl+Alt+m - create a method
            bus.load(60);
            bus.upload(110);
        } catch (InsufficiantCapacity | NotEnough e) {
            System.out.println(e);
        }

    }

    private static void brokenSomthing() {
        throw new Road66("broken tire");
    }
}
