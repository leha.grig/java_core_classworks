package com.alexgrig.lesson9_18_12_03_practics;

import java.util.Arrays;

public class Department {

    private final String name;
    private Group[] groups;

    public Department(String name) {
        this.name = name;
        groups = new Group[10];
    }

    public String getName() {
        return name;
    }

    public Group[] getGroups() {
        return groups;
    }

    public void addGroup(String name) {

// сделать одним циклом
        int index = -1;
        for (int i = 0; i < groups.length; i++) {
            if (groups[i] == null && index == -1){
                index = i;
            }
            if (groups[i].getName().equals(name)) {
                System.out.println("The group with name " + name + " is already exist");
                return;
            }
        }

            if (index != -1) {
                groups[index] = new Group(name);
            } else {
                Group[] temp = Arrays.copyOf(groups, groups.length * 2);
                temp[groups.length] = new Group(name);
                groups = temp;
            }

    }

    public boolean deleteGroup(Group group) {
        int count = 0;
        boolean check = false;
        for (int i = 0; i < groups.length; i++) {
            if (group.equals(groups[i])) {
                groups[i] = null;
                check = true;
            }
            if (groups[i] == null) {
                count++;
            }
        }
        if (check) {
            if (count > groups.length / 2) {
                Group[] temp = new Group[groups.length / 2];
                for (int i = 0, j = 0; i < groups.length; i++) {
                    if (groups[i] != null) {
                        temp[j] = groups[i];
                        j++;
                    }
                }
                groups = temp;
            }
            return true;
        }
        return false;
    }

    public void addStudent(Student student, Group group) {
        boolean check = true;
        for (int i = 0; i < groups.length; i++) {
            for (int j = 0; j < groups[i].getStudents().length; j++) {
                if (groups[i].getStudents()[j].equals(student)) {
                    System.out.println("this student is already present in " + groups[i].getName() + " group");
                    check = false;
                }
            }

        }
        if (check) {
            for (int i = 0; i < group.getStudents().length; i++) {
                if (group.getStudents()[i] == null) {
                    group.getStudents()[i] = student;
                    break;
                } else {
                    Student[] temp = Arrays.copyOf(group.getStudents(), group.getStudents().length * 2);
                    temp[group.getStudents().length] = student;
                    group.setStudents(temp);
                    break;
                }
            }
        }
    }

    public boolean deleteStudent(Student student, Group group) {
        int count = 0;
        boolean check = false;
        for (int i = 0; i < group.getStudents().length; i++) {
            if (group.getStudents()[i].equals(student)) {
                group.getStudents()[i] = null;
                check = true;
            }
            if (group.getStudents()[i] == null) {
                count++;
            }
        }
        if (check) {
            if (count > group.getStudents().length / 2) {
                Student[] temp = new Student[group.getStudents().length / 2];
                for (int i = 0, j = 0; i < group.getStudents().length; i++) {
                    if (group.getStudents()[i] != null) {
                        temp[j] = group.getStudents()[i];
                        j++;
                    }
                }
                group.setStudents(temp);
            }
            return true;
        }
        return false;
    }

    // если этот метод будет выводить номер студента в группе, то его можно будет использовать в пред. методах
    public boolean searchStudent(Student student, Group group) {
        for (int i = 0; i < group.getStudents().length; i++) {
            if (group.getStudents()[i].equals(student)) {
                return true;
            }
        }
        return false;
    }

    public Group compareGroups(Group group1, Group group2) {
        int mark1 = groupAverLevel(group1);
        int mark2 = groupAverLevel(group2);
        if (mark1 > mark2) {
            return group1;
        }
        if (mark1 < mark2) {
            return group2;
        }
        return null;
    }

    public int groupAverLevel(Group group) {
        int result = 0;
        int studCount = 0;
        for (int i = 0; i < group.getStudents().length; i++) {
            if (group.getStudents()[i] != null) {
                int studentAverMark = 0;
                int markCount = 0;
                for (Mark mark : group.getStudents()[i].getMarks()) {
                    if (mark.getMark() > 0) {
                        studentAverMark += mark.getMark();
                        markCount++;
                    }
                }
                studentAverMark = studentAverMark / markCount;
                studCount++;
                result += studentAverMark;
            }
        }
        result = result / studCount;
        return result;
    }

    @Override
    public String toString() {
        return "Department{" +
                "name='" + name + '\'' +
                ", groups=" + Arrays.toString(groups) +
                '}';
    }

}
