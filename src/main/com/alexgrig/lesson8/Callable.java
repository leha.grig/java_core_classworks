package com.alexgrig.lesson8;

public interface Callable {

    public static final int A = 10;
    public abstract void f();


    default void print(){
        System.out.println("a");
    }

    static void staticPrint(){
        System.out.println("static a");
    }
}

interface MyCallable extends Callable, Runnable {

}

