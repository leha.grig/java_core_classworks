package com.alexgrig.strings;

// Задание: преобразовывать строку так, чтоб в случае повторных символов повторы исчезали,
// а после первого встреченного данного символа вставлялось число повторов

public class StringProcessor {
    public static void main(String[] args) {
        String qwerty = process("qwertyyyyt");
        System.out.println(qwerty);

    }

    public static String process(String str) {
        String result = "";

        char[] chars = str.toCharArray();

        for (int i = 0; i < chars.length; i++) {
            if (chars[i] != '1') { // исключаем из рассмотрения символ '1', который используется как знак замены учтенного символа
                int counter = 1;
                for (int j = i + 1; j < chars.length; j++) {
                    if (chars[i] == chars[j]) {
                        ++counter;
                        chars[j] = '1'; // заменяем втсреченный повтор на символ '1'(считаем, что во входной строке их быть не может),
                        // чтоб при дальнейшем чтении строки этот символ уже не рассматривать
                    }
                }
                result += chars[i];
                if (counter>1){
                    result += counter;
                }

            }
        }
        return result;
    }
}

