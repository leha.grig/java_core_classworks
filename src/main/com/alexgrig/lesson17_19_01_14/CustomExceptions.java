package com.alexgrig.lesson17_19_01_14;

import java.io.IOException;

public class CustomExceptions {
    public static void main(String[] args) {
        try {
        throw new RTException("Awewdc", new IOException());
        } catch (RTException e){
            System.out.println(e.toString());
            System.out.println(e.getMessage());
            System.out.println(e.getCause());
        }
    }
    static class RTException extends RuntimeException {
        public RTException(String message, Exception exception) {
            super(message, exception);
        }
    }
}
