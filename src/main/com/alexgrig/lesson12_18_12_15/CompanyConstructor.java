package com.alexgrig.lesson12_18_12_15;

import java.util.Arrays;

public class CompanyConstructor {
    public static void main(String[] args) {
        Company company = new Company(Arrays.asList(
                new Employee(20, "Serg"),
                new Employee(25, "Alex"),
                new Employee(27, "Mary")
        ));

        for(Employee employee: company){
            System.out.println(employee);
        }

        for(Employee employee: company){
            System.out.println(employee);
        }
    }
}
