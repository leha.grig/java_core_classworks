package com.alexgrig.lesson8;

// как можно было бы создать класс Пресон с обязательными полями мама Персон и папа Персон -
// пустой приватный конструктор и метод-фабрика (паттерн "фабрика")

public class Person {

    private String name;
    //...
    private Person mother;
    private Person father;

    private Person() {
    }

    public static Person newInstance(String name, Person mother, Person father){
        if (mother == null || father == null){
            throw new IllegalArgumentException("can't create with empty parents");
        }
        Person result = new Person();
        result.name = name;
        result.father = father;
        return result;
    }






}

class Runner{


    public static void main(String[] args) {
        Person person = Person.newInstance(null, null, null);
    }
}

