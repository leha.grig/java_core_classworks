package com.alexgrig.lesson16_19_01_12.model;

import java.util.Objects;

public class Property {
    private String postcode;
    private PropertyType type;
    private int price;
    private Agent agent;

    public Property(String postcode, PropertyType type, int price, Agent agent) {
        this.postcode = postcode;
        this.type = type;
        this.price = price;
        this.agent = agent;
    }

    public String getPostcode() {
        return postcode;
    }

    public PropertyType getType() {
        return type;
    }

    public int getPrice() {
        return price;
    }

    public Agent getAgent() {
        return agent;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Property property = (Property) o;
        return price == property.price &&
                Objects.equals(postcode, property.postcode) &&
                type == property.type &&
                Objects.equals(agent, property.agent);
    }

    @Override
    public int hashCode() {
        return Objects.hash(postcode, type, price, agent);
    }

    enum PropertyType {
        HOUSE, FLAT
    }
}
