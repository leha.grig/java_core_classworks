package com.alexgrig.arrays01;

import java.util.Arrays;

public class App01 {
    public static void main(String[] args) {
        int[] arr = {1, 3, 5, 2, 27, 7, 31, 23, 17, 9, 13};
        int max = arr[0];
        for (int i = 1; i < arr.length; i++) {
            if (arr[i] > max) {
                max = arr[i];
            }
        }
        System.out.println(Arrays.toString(reverseArray(arr)));
    }

    static int getMax(int[] arr) {
        if (arr != null && arr.length > 0) {
            int max = arr[0];
            for (int i = 1; i < arr.length; i++) {
                if (arr[i] > max) {
                    max = arr[i];
                }
            }
            return max;
        }
        throw new IllegalArgumentException();
    }

    static boolean isDublicateInArray (int[] arr) {
        if (arr == null || arr.length == 0) {
            throw new IllegalArgumentException();
        }
        for (int i = 0; i<arr.length; i++ ){
            for (int j = i+1; j<arr.length; j++){
                if (arr[i] == arr[j]){
                    return true;
                }
            }
        }
        return false;
    }
    static boolean isDublicateInArray2 (int[] arr) { // оптимизированный метод, нет вложенных циклов, но нужно больше памяти
        if (arr == null || arr.length == 0) {
            throw new IllegalArgumentException();
        }
        boolean [] dArr = new boolean[getMax(arr)+1]; // создали булеан-массив, в котором количество элементов равно максимальному числу во
        // входном массиве. По умолчанию все значения false
        for (int i = 0; i<arr.length; i++ ){
            int temp = arr[i]; // временная переменная, равна значению числа в исходном массиве
            // если позиция в бул-массиве, равная значению числа в исходном массиве, содержит false (по умолчанию),
            // то меняем ее на true. Если содержит true - значит число в исходном массиве повторяется, метод возвращает true
            if (!dArr[temp]){
                dArr[temp] = true;
            }
            else {
                return true;
            }

        }
        return false;
    }
    static int [] reverseArray (int[] arr) { // инвертировать массив без создания нового временного (в исходном)
        int a = arr.length;
        int temp = 0;
        for (int i = 0; i<a/2; i++){
            temp = arr[i];
            arr[i] = arr[a-i-1];
            arr[a-i-1] = temp;
        }
        return arr;
    }
}
